<?php

//Import the constants from another file for security
require_once('secure/constants.php');


$action = $_GET['action'];
//Create functions that connects us to the database
function connection() {
		
	$conn = new mysqli(DBHOST, DBUSER, DBPASS, DBNAME);
	
	if($conn->connect_errno > 0){
    	die('Unable to connect to database [' . $conn->connect_error . ']');
	}
	
	return $conn;
}

if(isset($action) && $action == "login") {
		$username = $_POST['username'];
		$password = $_POST['password'];
		get_header_data($username, $password);
}


function get_header_data($username, $password) {
	
	$db = connection();
	$sql = "SELECT * FROM MyOdysseyUsers WHERE USERNAME = '$username' AND PASSWORD = '$password'";
	$arr = []; //Create an array to put your data in

	$result = $db->query($sql); //Note that $resut is a new variable, but it carries our a function right away

	while($row = $result->fetch_assoc()){
    	$arr[] = array (
			'id' => $row['ID']

		);
	}

	$json = json_encode($arr); 
	
	$result->free();
	$db->close();
	header_text($json);
} 

function header_text($data) {
	
	$array = json_decode($data, true);
	
	$output = "";
	
	for($i = 0; $i < count($array); $i++ ) {
		
		$output .= "<a href=\"../../profile.php?id=".$array[$i]["id"]."\">Go to My Profile</a>";
	}
	
	echo $output;
}




?>

