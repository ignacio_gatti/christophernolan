<?php

require_once('secure/constants.php');


  function connection() {
    
    $conn = new mysqli(DBHOST, DBUSER, DBPASS, DBNAME);
    
    if ($conn->connect_errno > 0){
      die('Sorry, unable to connect to the database [' . $conn->connect_error . ']');
    }
    
    return $conn;
    
  }

  

  function get_all_data(){
    
    $db = connection();
    $sql = "SELECT * FROM MyOdysseyUsers";
    $arr = [];
    
    $result = $db->query($sql);
    
    if(!$result){
      die('There was an error running the query [' . $db-error . ']');
    }
    
    while($row = $result->fetch_assoc()){
      $arr = array (
      'id' => $row['ID'],
      'username' => $row['USERNAME'],
      'password' => $row['PASSWORD'],
      'fname' => $row['FNAME'],
      'lname' => $row['LNAME'],
      'address' => $row['ADDRESS'],
      'job' => $row['JOB'],
      'email' => $row['EMAIL'],
      'phone' => $row['PHONE'],
      'cell' => $row['CELL']      
      
      );
      
    }
           
    $json = json_encode(array("data" => $arr));
    
    print_r($json);
    
    $result->free();
    $db->close();
    
    
    
    
    
    
  }

function showMeText($id, $whereSection){
   $db = connection();
    $sql = "SELECT ".$whereSection." FROM MyOdysseyUsers WHERE ID = ".$id;
    $arr = [];
    
    $result = $db->query($sql);
  
  while($row = $result->fetch_assoc()){
      $arr = array (
      'content' => $row[$whereSection],
        );
      
    }
  
  return $arr['content'];
  
}







?>