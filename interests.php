<?php include_once('php/functions/functions.php'); 
session_start();
?>
  
<!doctype html>
<html>

<head>

		<link rel="stylesheet" type="text/css" href="css/main.css">
		<script src=""></script>

<title>MyOdyssey | INTERESTS</title>

</head>

<body onload="visible(), visible2()" >

<header></header>
<section class="whole_block">

	<ul>
		<li><a class="navi" href="profile.php?id=<?php echo $_GET['id']; ?>">Home </a></li>
		<li><a class="navi" href="interests.php?id=<?php echo $_GET['id']; ?>">My Interests </a></li>
		<li><a class="navi" href="manage.php?id=<?php echo $_GET['id']; ?>">Manage Account </a></li>
	</ul> 

<!-- <aside></aside> -->
          <section class="main_block"><!--------------BEGINNING OF main_block------------------------>
							<form action="index.php" >
							<input type="submit" value="Log Out" class="logout">
							<?php session_unset();
							session_destroy();	
							?>	
							</form>
            <h2>My Interests, Hobbies and Education	</h2>
              <section  class="center_block"><!------------BEGINNING OF view_block-------------------->
								<section class="left_center_block"><!------------BEGINNING OF left_center_block-------------------->
              		
									<!---------------------------------------INTEREST BLOCK 001 ------------------------------------------>
																							<!--IMAGE SECTION -->
									<form action="update_image.php?action=update&col=INT_IMG01&ref='../interests.php'&nm=int_img01" method="POST">
										<input type="hidden" name="id" value="<?php echo $_GET['id']; ?>" >
										<div class="image_block"><!------------BEGINNING OF image_block-------------------->
										<img src="<?php echo showMeText($_GET['id'], "INT_IMG02"); ?>" alt="<?php echo showMeText($_GET['id'], "INT_IMG01"); ?>" class="img_square">
										<input type="submit" value="Update Photo" id="submitImage">
									</div><!------------END OF image_block-------------------->
									</form>
									
																							<!--TEXT SECTION -->
									<form action="update_small_text.php?action=update&col=INT_TXT01&ref='../interests.php'&nm=int_txt01" method="POST">
										<input type="hidden" name="id" value="<?php echo $_GET['id']; ?>" >
										"<div class="small_text_box"><?php echo showMeText($_GET['id'], "INT_TXT01"); ?></div>
										<input type="submit" value="Update Text" class="submitText">
									</form>
									
									
									
									
									
									
                  
									<!---------------------------------------INTEREST BLOCK 002 ------------------------------------------>
                  <!--IMAGE SECTION -->
									<form action="update_image.php?action=update&col=INT_IMG02&ref='../interests.php'&nm=int_img02" method="POST">
									<input type="hidden" name="id" value="<?php echo $_GET['id']; ?>" >
										<div class="image_block"><!------------BEGINNING OF image_block-------------------->
										<img src="<?php echo showMeText($_GET['id'], "INT_IMG02"); ?>" alt="<?php echo showMeText($_GET['id'], "INT_IMG02"); ?>" class="img_square"> 
										<input type="submit" value="Update Photo">
										</div><!------------END OF image_block-------------------->
									</form>
									
									
													<!--TEXT SECTION -->
									<form action="update_small_text.php?action=update&col=INT_TXT02&ref='../interests.php'&nm=int_txt02" method="POST">
									<div class="small_text_box"><?php echo showMeText($_GET['id'], "INT_TXT02"); ?></div>
									<input type="hidden" name="id" value="<?php echo $_GET['id']; ?>" >
									<input type="submit" value="Update Text" class="submitText">
									</form>
									
									
									
									
									 
                  
									<!---------------------------------------INTEREST BLOCK 003 ------------------------------------------>
                  <!--IMAGE SECTION -->
									<form action="update_image.php?action=update&col=INT_IMG03&ref='../interests.php'&nm=int_img03" method="POST">
									<input type="hidden" name="id" value="<?php echo $_GET['id']; ?>" >
									<div class="image_block"><!------------BEGINNING OF image_block-------------------->
										<img src="<?php echo showMeText($_GET['id'], "INT_IMG03"); ?>" alt="<?php echo showMeText($_GET['id'], "INT_IMG03"); ?>" class="img_square"> 
										<input type="submit" value="Update Photo">
									</div><!------------END OF image_block-------------------->
									</form>
									
													<!--TEXT SECTION -->
									<form action="update_small_text.php?action=update&col=INT_TXT03&ref='../interests.php'&nm=int_txt03" method="POST">
										<input type="hidden" name="id" value="<?php echo $_GET['id']; ?>" >
										<div class="small_text_box"><?php echo showMeText($_GET['id'], "INT_TXT03"); ?></div>
										<input type="submit" value="Update Text" class="submitText">
										<input type="hidden" name="id" value="<?php echo $_GET['id']; ?>" >
									</form>
									
									
									
									
									
									
									
									
                  
									<!---------------------------------------INTEREST BLOCK 004 ------------------------------------------>
                  <!--IMAGE SECTION -->
										<form action="update_image.php?action=update&col=INT_IMG04&ref='../interests.php'&nm=int_img04" method="POST">
										<div class="image_block"><!------------BEGINNING OF image_block-------------------->
										<img src="<?php echo showMeText($_GET['id'], "INT_IMG04"); ?>" alt="<?php echo showMeText($_GET['id'], "INT_IMG04"); ?>" class="img_square"> 
										<input type="submit" value="Update Photo">
										<input type="hidden" name="id" value="<?php echo $_GET['id']; ?>" >	
									</div><!------------END OF image_block-------------------->
									</form>
									
									
											<!--TEXT SECTION -->
									<form action="update_small_text.php?action=update&col=INT_TXT04&ref='../interests.php'&nm=int_txt04" method="POST">
										<input type="hidden" name="id" value="<?php echo $_GET['id']; ?>" >
										<div class="small_text_box"><?php echo showMeText($_GET['id'], "INT_TXT04"); ?></div>
										<input type="submit" value="Update Text" class="submitText">
									</form>
									 
                  
									  
									
									 
									 
									
									
								
								
								</section><!------------END OF left_center_block-------------------->
<!------------------------------------------------------------------------------------------------------------------------------->
								
            		<section class="right_center_block"><!------BEGINNING OF right_center_block------------->
								
									
									
									
									
									
								<!---------------------------------------INTEREST BLOCK 005 ------------------------------------------>	
								<!--IMAGE SECTION -->
									<form action="update_image.php?action=update&col=INT_IMG05&ref='../interests.php'&nm=int_img05" method="POST">
									<div class="image_block"><!------------BEGINNING OF image_block-------------------->
										<img src="<?php echo showMeText($_GET['id'], "INT_IMG05"); ?>" alt="<?php echo showMeText($_GET['id'], "INT_IMG05"); ?>" class="img_square"> 
									<input type="submit" value="Update Photo">
									<input type="hidden" name="id" value="<?php echo $_GET['id']; ?>" >	
									</div><!------------END OF image_block-------------------->
									</form>
									
									
									<!--TEXT SECTION -->
									<form action="update_small_text.php?action=update&col=INT_TXT05&ref='../interests.php'&nm=int_txt05" method="POST">
										<input type="hidden" name="id" value="<?php echo $_GET['id']; ?>" >
										<div class="small_text_box"><?php echo showMeText($_GET['id'], "INT_TXT05"); ?></div>
										<input type="submit" value="Update Text" class="submitText">
									</form>
									
            			
									
									
									
									
									<!---------------------------------------INTEREST BLOCK 006 ------------------------------------------>
                  <!--IMAGE SECTION -->
									<form action="update_image.php?action=update&col=INT_IMG06&ref='../interests.php'&nm=int_img06" method="POST">
										<div class="image_block"><!------------BEGINNING OF image_block-------------------->
										<img src="<?php echo showMeText($_GET['id'], "INT_IMG06"); ?>" alt="<?php echo showMeText($_GET['id'], "INT_IMG06"); ?>" class="img_square">
										<input type="submit" value="Update Photo">
										<input type="hidden" name="id" value="<?php echo $_GET['id']; ?>" >	
									</div><!------------END OF image_block-------------------->
									</form>
									
									
									<!--TEXT SECTION -->
									<form action="update_small_text.php?action=update&col=INT_TXT06&ref='../interests.php'&nm=int_txt06" method="POST">
										<input type="hidden" name="id" value="<?php echo $_GET['id']; ?>" >
										<div class="small_text_box"><?php echo showMeText($_GET['id'], "INT_TXT06"); ?></div>
										<input type="submit" value="Update Text" class="submitText">
									</form>
									
									
									
									
									
									
                  <!---------------------------------------INTEREST BLOCK 007 ------------------------------------------>
                  <!--IMAGE SECTION -->
									<form action="update_image.php?action=update&col=INT_IMG07&ref='../interests.php'&nm=int_img07" method="POST">
										<div class="image_block"><!------------BEGINNING OF image_block-------------------->
										<img src="<?php echo showMeText($_GET['id'], "INT_IMG07"); ?>" alt="<?php echo showMeText($_GET['id'], "INT_IMG07"); ?>" class="img_square"> 
										<input type="submit" value="Update Photo">
										<input type="hidden" name="id" value="<?php echo $_GET['id']; ?>" >	
									</div><!------------END OF image_block-------------------->
									</form>
									
									
									<!--TEXT SECTION -->
									<form action="update_small_text.php?action=update&col=INT_TXT07&ref='../interests.php'&nm=int_txt07" method="POST">
										<input type="hidden" name="id" value="<?php echo $_GET['id']; ?>" >
										<div class="small_text_box"><?php echo showMeText($_GET['id'], "INT_TXT07"); ?></div>
										<input type="submit" value="Update Text" class="submitText">
									</form>
									
									
									
									
									
									
									
                  <!---------------------------------------INTEREST BLOCK 008 ------------------------------------------>
                  <!--IMAGE SECTION -->
									<form action="update_image.php?action=update&col=INT_IMG08&ref='../interests.php'&nm=int_img08" method="POST">
										<div class="image_block"><!------------BEGINNING OF image_block-------------------->
										<img src="<?php echo showMeText($_GET['id'], "INT_IMG08"); ?>" alt="<?php echo showMeText($_GET['id'], "INT_IMG08"); ?>" class="img_square"> 
										<input type="submit" value="Update Photo">
										<input type="hidden" name="id" value="<?php echo $_GET['id']; ?>" >
									</div><!------------END OF image_block-------------------->
									</form>
										
										
									<!--TEXT SECTION -->
									<form action="update_small_text.php?action=update&col=INT_TXT08&ref='../interests.php'&nm=int_txt08" method="POST">
										<input type="hidden" name="id" value="<?php echo $_GET['id']; ?>" >
										<div class="small_text_box"><?php echo showMeText($_GET['id'], "INT_TXT08"); ?></div>
										<input type="submit" value="Update Text" class="submitText">
									</form>
									
					
								
								
								
								
								
								</section><!------------END OF right_center_block-------------------->
								
<!-- 								<section class="center_center_block"><!------------BEGINNING OF center_center_block--------------------> 
<!-- 								
								
								
								
								
								
								
								
								
								
								
								
								</section><!------------END OF center_center_block-------------------->
          <h1>

          </h1>



          <p>


          </p>

                </section><!----------------------END OF view_block---------------------------->

            </section><!----------------------END OF main_block---------------------------->
  </section>
	<script src="js/script.js"></script>
</body>





</html>