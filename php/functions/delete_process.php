<?php

//Import the constants from another file for security
require_once('secure/constants.php');

$action = $_GET['action'];
$col = $_GET['col'];
$ref = $_GET['ref'];
$id = $_GET['id'];
////////////////////////////////////
////////////////////////////////////
////////////////////////////////////

if(isset($action) && $action == "delete") {
	if(isset($action) && $col == "SUMMARY"){	
	
	$id = $_GET['id'];
	$summ = $_POST['summary'];
	delete_data($id, $summ, $col);
	}
	
	else if(isset($action) && $col == "EDUCATION"){
		$id = $_GET['id'];
		$educ = $_POST['education'];
		delete_data($id, $educ, $col);		
	}
	
	else if(isset($action) && $col == "CURRENTLY"){
		$id = $_GET['id'];
		$curr = $_POST['currently'];
		delete_data($id, $curr, $col);		
	}
	
	else if(isset($action) && $col == "USERNAME"){
		$id = $_GET['id'];
		$usrname = $_POST['username'];
		delete_data($id, $usrname, $col);		
	}

		else if(isset($action) && $col == "PASSWORD"){
		$id = $_GET['id'];
		$passw = $_POST['password'];
		delete_data($id, $passw, $col);		
	}
	else if(isset($action) && $col == "FNAME"){
		$id = $_GET['id'];
		$finame = $_POST['fname'];
		delete_data($id, $finame, $col);		
	}	

	else if(isset($action) && $col == "LNAME"){
		$id = $_GET['id'];
		$laname = $_POST['lname'];
		delete_data($id, $laname, $col);		
	}	
	
		else if(isset($action) && $col == "ADDRESS"){
		$id = $_GET['id'];
		$adrss = $_POST['address'];
		delete_data($id, $adrss, $col);		
	}		
	
		else if(isset($action) && $col == "JOB"){
		$id = $_GET['id'];
		$jobtt = $_POST['job'];
		delete_data($id, $jobtt, $col);		
	}			

		else if(isset($action) && $col == "EMAIL"){
		$id = $_GET['id'];
		$e_mail = $_POST['email'];
		delete_data($id, $e_mail, $col);		
	}	
	
		else if(isset($action) && $col == "PHONE"){
		$id = $_GET['id'];
		$phno = $_POST['phone'];
		delete_data($id, $phno, $col);		
	}	

		else if(isset($action) && $col == "CELL"){
		$id = $_GET['id'];
		$cellno = $_POST['cell'];
		delete_data($id, $cellno, $col);		
	}	

		else if(isset($action) && $col == "PROF_IMG"){
		$id = $_GET['id'];
		$prof_img = $_POST['prof_img'];
		delete_data($id, $prof_img, $col);		
	}	
		else if(isset($action) && $col == "INT_IMG01"){
		$id = $_GET['id'];
		$int_img01 = $_POST['int_img01'];
		delete_data($id, $int_img01, $col);	
		
	}		

		else if(isset($action) && $col == "INT_IMG02"){
		$id = $_GET['id'];
		$int_img02 = $_POST['int_img02'];
		delete_data($id, $int_img02, $col);		
	}		

		else if(isset($action) && $col == "INT_IMG03"){
		$id = $_GET['id'];
		$int_img03 = $_POST['int_img03'];
		delete_data($id, $int_img03, $col);		
	}	

		else if(isset($action) && $col == "INT_IMG04"){
		$id = $_GET['id'];
		$int_img04 = $_POST['int_img04'];
		delete_data($id, $int_img04, $col);		
	}	

		else if(isset($action) && $col == "INT_IMG05"){
		$id = $_GET['id'];
		$int_img05 = $_POST['int_img05'];
		delete_data($id, $int_img05, $col);		
	}	
	
		else if(isset($action) && $col == "INT_IMG06"){
		$id = $_GET['id'];
		$int_img06 = $_POST['int_img06'];
		delete_data($id, $int_img06, $col);		
	}	
	
		else if(isset($action) && $col == "INT_IMG07"){
		$id = $_GET['id'];
		$int_img07 = $_POST['int_img07'];
		delete_data($id, $int_img07, $col);		
	}	
	
		else if(isset($action) && $col == "INT_IMG08"){
		$id = $_GET['id'];
		$int_img08 = $_POST['int_img08'];
		delete_data($id, $int_img08, $col);		
	}	
	
		else if(isset($action) && $col == "INT_TXT01"){
		$id = $_GET['id'];
		$int_txt01 = $_POST['int_txt01'];
		delete_data($id, $int_txt01, $col);		
	}	
	
		else if(isset($action) && $col == "INT_TXT02"){
		$id = $_GET['id'];
		$int_txt02 = $_POST['int_txt02'];
		delete_data($id, $int_txt02, $col);		
	}	
	
		else if(isset($action) && $col == "INT_TXT03"){
		$id = $_GET['id'];
		$int_txt03 = $_POST['int_txt03'];
		delete_data($id, $int_txt03, $col);		
	}	
	
		else if(isset($action) && $col == "INT_TXT04"){
		$id = $_GET['id'];
		$int_txt04 = $_POST['int_txt04'];
		delete_data($id, $int_txt04, $col);		
	}	
	
		else if(isset($action) && $col == "INT_TXT05"){
		$id = $_GET['id'];
		$int_txt05 = $_POST['int_txt05'];
		delete_data($id, $int_txt05, $col);		
	}	
	
		else if(isset($action) && $col == "INT_TXT06"){
		$id = $_GET['id'];
		$int_txt06 = $_POST['int_txt06'];
		delete_data($id, $int_txt06, $col);		
	}	
	
		else if(isset($action) && $col == "INT_TXT07"){
		$id = $_GET['id'];
		$int_txt07 = $_POST['int_txt07'];
		delete_data($id, $int_txt07, $col);		
	}	
	
		else if(isset($action) && $col == "INT_TXT08"){
		$id = $_GET['id'];
		$int_txt08 = $_POST['int_txt08'];
		delete_data($id, $int_txt08, $col);		
	}	
}



else {
	$action = 0;
}

////////////////////////////////////
////////////////////////////////////
////////////////////////////////////

//Create functions that connects us to the database
function connection() {
		
	$conn = new mysqli(DBHOST, DBUSER, DBPASS, DBNAME);
	
	if($conn->connect_errno > 0){
    	die('Unable to connect to database [' . $conn->connect_error . ']');
	}
	
	return $conn;
}

////////////////////////////////////
////////////////////////////////////
////////////////////////////////////



function delete_data($id, $opt, $col) {
	
	$db = connection();
	
	$id			=	$db->real_escape_string($id);
	$opt		=	$db->real_escape_string($opt);
	$col		=	$db->real_escape_string($col);
	
	$sql = "DELETE FROM MyOdysseyUsers SET $col = '$opt' WHERE ID = ".$id;
	
	$result = $db->query($sql);
	
	$count = $db->affected_rows;
	
	if(!$result){
		print_r($sql);
		echo "<br><br>";
		print_r($id);
		echo "<br><br>";
		print_r($opt);
		echo "<br><br>";		
		print_r($col);
		echo "<br><br>";		
		echo $count;
		die($db->error);
	}
	
	if($count == 1) {
		header('Location: ../../profile.php?id='.$id);
	}
	
	//Insert function does not return anything, but it can give us a count of the number of records added, which should be one
	//When that happens we will be redirected to the homepage
}

////////////////////////////////////
////////////////////////////////////
////////////////////////////////////



?>